@extends('layouts.master')

@section('pageTitle', 'Register Payment')

@section('navigation')
	<a class="btn btn-default" href="{{ route('students.show', $studentId) }}">Back to Student</a>
@endsection

@section('content')

	<h2>New Payment for Flint Lockwood</h2>

	@if (Session::has('error'))
		<div class="alert alert-danger">{{ Session::get('error') }}</div>
	@endif

	<form action="{{ route('payments.store', $studentId) }}" method="post">

		<div class="form-group">
			<label for="amount">Amount</label>
			<input class="form-control" type="number" step="any" name="amount" id="amount" required autofocus value="{{ Input::old('amount') }}"/>
		</div>

		<div class="form-group">
			<label for="date">Date</label>
			<input class="form-control" type="text" name="date" id="date" pattern="[0-9]{4}\-[0-9]{2}\-[0-9]{2}" required value="{{ Input::old('date') }}"/>
			<p class="help-block">Please, fill in format YYYY-MM-DD</p>
		</div>

		<button type="submit" class="btn btn-primary">Save</button>

	</form>

@endsection


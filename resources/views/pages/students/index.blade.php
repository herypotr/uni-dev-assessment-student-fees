@extends('layouts.master')

@section('pageTitle', 'Students')

@section('content')

	<table class="table table-striped">
		<thead>
			<tr>
				<th>Id</th>
				<th>Name</th>
			</tr>
		</thead>
		<tbody>
			@forelse($students as $student)
				<tr>
					<td>{{ $student->getId()->asInteger() }}</td>
					<td>
						<a href="{{ route('students.show', $student->getId()->asInteger()) }}">
							{{ $student->getForename() }} {{ $student->getSurname() }}
						</a>
					</td>
				</tr>
			@empty
				<tr>
					<td class="text-center" colspan="2">No items found.</td>
				</tr>
			@endforelse
		</tbody>
	</table>

@endsection


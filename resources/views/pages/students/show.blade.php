@extends('layouts.master')

@section('pageTitle', $student->getForename() . ' ' . $student->getSurname())

@section('navigation')
	<a class="btn btn-default" href="{{ route('students.index') }}">Back to Students</a>
@endsection

@section('content')

	<h2>Payments</h2>
	<p class="text-right">
		<a class="btn btn-primary btn-sm" href="{{ route('payments.create', $student->getId()->asInteger()) }}">Register new Payment</a>
	</p>

	<table class="table table-striped">
		<thead>
			<tr>
				<th>Id</th>
				<th>Amount</th>
				<th>Received on</th>
			</tr>
		</thead>
		<tbody>
			@forelse($payments as $payment)
				<tr>
					<td>{{ $payment->getId()->asInteger() }}</td>
					<td>&pound;{{ number_format($payment->getAmount()->asDecimal(), 2) }}</td>
					<td>{{ $payment->getDate()->format('j/n/Y') }}</td>
				</tr>
			@empty
				<tr>
					<td class="text-center" colspan="3">No payments registered.</td>
				</tr>
			@endforelse
		</tbody>
	</table>

@endsection


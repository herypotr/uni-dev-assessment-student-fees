<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<meta name="description" content="Assessment for UoN">
	<meta name="author" content="Harry Birch <contact@harrybirch.co.uk>">
	{{--<link rel="icon" href="{{ asset('favicon.ico') }}">--}}

	<title>@yield('pageTitle')</title>

	<!-- Bootstrap core CSS -->
	<link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
	<link href="{{ asset('css/styles.css') }}" rel="stylesheet">
</head>

<body>

<div class="container">
	<header class="header clearfix">
		<h1 class="pull-left">@yield('pageTitle')</h1>
		<div class="pull-right">
			@yield('navigation')
		</div>
	</header>

	<section class="content">
		@yield('content')
	</section>

	<footer class="footer clearfix">
		<p class="pull-left">Test Assessment for University of Nottingham by Harry Birch on 25/5/2015</p>
		<a class="btn btn-default btn-xs pull-right" href="{{ route('settings.reset') }}">Reset Session Data</a>
	</footer>

</div> <!-- /container -->

</body>
</html>

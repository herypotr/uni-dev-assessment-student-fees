## Developer Assessment – Student Fees

### Overview

#### Brief
Administrative interface for a system that will allow the university to keep track of the fees paid by students.

#### Execution
Domain consists of Entities (Student, Payment), their respective Repositories + Gateways (Session based) and several essential ValueObjects (Id, Price, Date, Collection).

Controller and View layers are provided by [Laravel framework](http://laravel.com/), as well as routing, DI/IoC resolution and PHPUnit test setup.


### Installation

1) Run

	composer install

2) Make sure `storage` folder is writable

	chmod -R 0777 storage

3) Point your VHost/Vagrant config to `public`

### Tests

Application includes PHPUnit tests of the Domain - ValueObjects, Entities, Repositories, Gateways.

To run all Unit tests, run (without alias or phpunit installed globally)

	vendor/phpunit/phpunit/phpunit
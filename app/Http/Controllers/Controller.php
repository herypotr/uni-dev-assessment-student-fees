<?php namespace App\Http\Controllers;

use App\Commands\SeedSessionData;
use Illuminate\Foundation\Bus\DispatchesCommands;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Support\Facades\Session;

abstract class Controller extends BaseController {

	use DispatchesCommands, ValidatesRequests;

	/**
	 * Seeds the data if it's the first we run this app
	 */
	public function __construct()
	{
	    if (!Session::get('persistence', false))
		{
			$this->dispatch(new SeedSessionData());
		}
	}
}

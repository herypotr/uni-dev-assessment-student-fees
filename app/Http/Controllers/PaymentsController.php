<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Input;
use Illuminate\View\View;
use StudentFees\Payment\Payment;
use StudentFees\Payment\PaymentRepository;
use StudentFees\Shared\ValueObjects\Date;
use StudentFees\Shared\ValueObjects\Id;
use StudentFees\Shared\ValueObjects\Price;

class PaymentsController extends Controller {

	/**
	 * Show the form for creating a new resource.
	 *
	 * @param int $studentId
	 * @return View
	 */
	public function create($studentId)
	{
		return view('pages.payments.create')
			->with('studentId', $studentId);
	}

	/**
	 * Store a newly created resource in storage.
	 * TODO: use the Builder/Factory here as well to create entity from array
	 *
	 * @param $studentId
	 * @param PaymentRepository $paymentRepository
	 * @return Response
	 */
	public function store($studentId, PaymentRepository $paymentRepository)
	{
		try
		{
			// persist
			$payment = new Payment();
			$payment->setStudentId(Id::make((int) $studentId));
			$payment->setAmount(Price::make(Input::get('amount')));
			$payment->setDate(Date::make(Input::get('date')));

			$paymentRepository->save($payment);
		} catch (\Exception $e)
		{
			return redirect()->back()->with('error', $e->getMessage())->withInput(Input::all());
		}

		return redirect()->route('students.show', $studentId);
	}

}

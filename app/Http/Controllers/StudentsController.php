<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Illuminate\View\View;
use StudentFees\Payment\PaymentRepository;
use StudentFees\Shared\ValueObjects\Id;
use StudentFees\Student\StudentRepository;

class StudentsController extends Controller {

	/**
	 * @var StudentRepository
	 */
	private $studentRepository;

	public function __construct(StudentRepository $studentRepository)
	{
		$this->studentRepository = $studentRepository;

		parent::__construct();
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return View
	 */
	public function index()
	{
		$students = $this->studentRepository->findAll();

		return view('pages.students.index')
			->with('students', $students);
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int $id
	 * @param PaymentRepository $paymentRepository
	 * @return View
	 */
	public function show($id, PaymentRepository $paymentRepository)
	{
		$student = $this->studentRepository->findById(Id::make((int)$id));
		$payments = $paymentRepository->findByStudentId($student->getId());

		return view('pages.students.show')
			->with('student', $student)
			->with('payments', $payments);
	}

}

<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function() {
	return redirect()->to('students');
});

Route::get('students', ['uses' => 'StudentsController@index', 'as' => 'students.index']);
Route::get('students/{id}', ['uses' => 'StudentsController@show', 'as' => 'students.show']);
Route::get('students/{studentId}/payments/create', ['uses' => 'PaymentsController@create', 'as' => 'payments.create']);
Route::post('students/{studentId}/payments', ['uses' => 'PaymentsController@store', 'as' => 'payments.store']);

Route::get('settings/reset', ['as' => 'settings.reset', function() {
	Session::forget('persistence');
	return redirect()->route('students.index');
}]);
<?php namespace App\Commands;

use Illuminate\Contracts\Bus\SelfHandling;
use Illuminate\Support\Facades\App;
use StudentFees\Payment\Payment;
use StudentFees\Payment\PaymentRepository;
use StudentFees\Shared\ValueObjects\Date;
use StudentFees\Shared\ValueObjects\Id;
use StudentFees\Shared\ValueObjects\Price;
use StudentFees\Student\Student;
use StudentFees\Student\StudentRepository;

class SeedSessionData extends Command implements SelfHandling {

	/**
	 * @var StudentRepository
	 */
	private $studentRepository;
	/**
	 * @var PaymentRepository
	 */
	private $paymentRepository;

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		$this->studentRepository = App::make('StudentFees\Student\StudentRepository');
		$this->paymentRepository = App::make('StudentFees\Payment\PaymentRepository');
	}

	/**
	 * Seed the session with our data
	 *
	 * @return void
	 */
	public function handle()
	{
		$students = [
			['forename' => 'Flint', 'surname' => 'Lockwood'],
			['forename' => 'Sam', 'surname' => 'Sparks'],
			['forename' => 'Steve', 'surname' => 'Lockwood'],
			['forename' => 'Earl', 'surname' => 'Devereaux'],
			['forename' => 'Brent', 'surname' => 'McHale'],
		];

		foreach ($students as $studentData)
		{
			$student = new Student();
			$student->setForename($studentData['forename']);
			$student->setSurname($studentData['surname']);

			$this->studentRepository->save($student);
		}

		$payments = [
			['student_id' => 3, 'amount' => 9000, 'date' => '2014-06-03'],
			['student_id' => 2, 'amount' => 9000, 'date' => '2014-03-08'],
			['student_id' => 2, 'amount' => 9000, 'date' => '2014-05-04'],
		];

		foreach ($payments as $paymentData)
		{
			$payment = new Payment();
			$payment->setStudentId(Id::make($paymentData['student_id']));
			$payment->setAmount(Price::make($paymentData['amount']));
			$payment->setDate(Date::make($paymentData['date']));

			$this->paymentRepository->save($payment);
		}

	}

}

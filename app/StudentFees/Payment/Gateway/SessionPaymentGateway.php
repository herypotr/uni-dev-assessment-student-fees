<?php namespace StudentFees\Payment\Gateway;

use Illuminate\Support\Facades\Session;
use StudentFees\Shared\Gateway\AbstractSessionGateway;
use StudentFees\Shared\Gateway\SessionGateway;
use StudentFees\Shared\ValueObjects\Id;

class SessionPaymentGateway extends AbstractSessionGateway implements SessionGateway {

	/**
	 * @var string
	 */
	protected $sessionKey = 'persistence.payments';

	/**
	 * Return all records that match supplied Student Id
	 * @param Id $id
	 * @return array
	 */
	public function findByStudentId(Id $id)
	{
		$collection = Session::get($this->sessionKey, []);

		if (!$collection) return [];

		return array_filter($collection, function ($record) use ($id)
		{
			return $record['student_id'] == $id->asInteger();
		});
	}

}
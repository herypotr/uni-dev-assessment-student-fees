<?php namespace StudentFees\Payment;

use StudentFees\Shared\Entity\AbstractEntity;
use StudentFees\Shared\ValueObjects\Date;
use StudentFees\Shared\ValueObjects\Id;
use StudentFees\Shared\ValueObjects\Price;

class Payment extends AbstractEntity {

	/**
	 * @var Id
	 */
	protected $studentId;

	/**
	 * @var Price
	 */
	protected $amount;

	/**
	 * @var Date
	 */
	protected $date;

	/**
	 * @return Id
	 */
	public function getStudentId()
	{
		return $this->studentId;
	}

	/**
	 * @param Id $studentId
	 */
	public function setStudentId($studentId)
	{
		$this->studentId = $studentId;
	}

	/**
	 * @return Price
	 */
	public function getAmount()
	{
		return $this->amount;
	}

	/**
	 * @param Price $amount
	 */
	public function setAmount($amount)
	{
		$this->amount = $amount;
	}

	/**
	 * @return Date
	 */
	public function getDate()
	{
		return $this->date;
	}

	/**
	 * @param Date $date
	 */
	public function setDate($date)
	{
		$this->date = $date;
	}

}
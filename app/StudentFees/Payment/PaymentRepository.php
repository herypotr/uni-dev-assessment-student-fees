<?php  namespace StudentFees\Payment;

use StudentFees\Shared\Repository\Repository;
use StudentFees\Shared\ValueObjects\Collection;
use StudentFees\Shared\ValueObjects\Id;

interface PaymentRepository extends Repository {

	/**
	 * Returns all records found by supplied Student Id
	 * @param Id $studentId
	 * @return Collection
	 */
	public function findByStudentId(Id $studentId);
}
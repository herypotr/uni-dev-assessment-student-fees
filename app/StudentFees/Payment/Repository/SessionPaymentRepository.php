<?php namespace StudentFees\Payment\Repository;

use StudentFees\Payment\Gateway\SessionPaymentGateway;
use StudentFees\Payment\Payment;
use StudentFees\Payment\PaymentRepository;
use StudentFees\Shared\Repository\AbstractSessionRepository;
use StudentFees\Shared\ValueObjects\Collection;
use StudentFees\Shared\ValueObjects\Date;
use StudentFees\Shared\ValueObjects\Id;
use StudentFees\Shared\ValueObjects\Price;

class SessionPaymentRepository extends AbstractSessionRepository implements PaymentRepository {

	/**
	 * @param SessionPaymentGateway $gateway
	 */
	public function __construct(SessionPaymentGateway $gateway)
	{
		$this->gateway = $gateway;
	}

	/**
	 * Returns all records found by supplied Student Id
	 * @param Id $studentId
	 * @return Collection
	 */
	public function findByStudentId(Id $studentId)
	{
		if (!$records = $this->gateway->findByStudentId($studentId))
		{
			return Collection::make([]);
		}

		$results = [];
		foreach ($records as $record)
		{
			$results[] = $this->createEntityFromRecord($record);
		}

		return Collection::make($results);

	}

	/**
	 * @param $record
	 * @return mixed
	 */
	protected function createEntityFromRecord($record)
	{
		$entity = new Payment();

		$entity->setId(Id::make($record['id']));
		if ($record['student_id']) $entity->setStudentId(Id::make($record['student_id']));

		$entity->setAmount(Price::make((int)$record['amount']));
		$entity->setDate(Date::make($record['date']));

		return $entity;
	}

}
<?php namespace StudentFees\Shared\Repository;

use StudentFees\Shared\Entity\AbstractEntity;
use StudentFees\Shared\Gateway\SessionGateway;
use StudentFees\Shared\ValueObjects\Collection;
use StudentFees\Shared\ValueObjects\Id;

abstract class AbstractSessionRepository {

	/**
	 * @var SessionGateway
	 */
	protected $gateway;

	/**
	 * Returns 1 record
	 * @param Id $id
	 * @return mixed
	 */
	public function findById(Id $id)
	{
		return $this->createEntityFromRecord($this->gateway->find($id->asInteger()));
	}

	/**
	 * @return Collection
	 */
	public function findAll()
	{
		if (!$records = $this->gateway->findAll())
		{
			return Collection::make([]);
		}

		$results = [];
		foreach ($records as $record)
		{
			$results[] = $this->createEntityFromRecord($record);
		}

		return Collection::make($results);
	}

	/**
	 * Persists Entity
	 * @param AbstractEntity $entity
	 * @return mixed
	 */
	public function save($entity)
	{
		$record = $entity->toArray();

		// update
		if ($record['id'])
		{
			$record = $this->gateway->update($record, $record['id']);

			return $this->createEntityFromRecord($record);
		}

		// insert + set Id
		$record = $this->gateway->insert($record);
		$entity = $this->createEntityFromRecord($record);

		return $entity;
	}

	/**
	 * FIX: breaking SRP here, needs an entity builder to create entity from record
	 * @param $record
	 * @return mixed
	 */
	protected function createEntityFromRecord($record)
	{
		throw new \Exception('Repository missing createEntityFromRecord override.');
	}

}
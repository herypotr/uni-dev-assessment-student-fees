<?php  namespace StudentFees\Shared\Repository;

use StudentFees\Shared\ValueObjects\Collection;
use StudentFees\Shared\ValueObjects\Id;

interface Repository {

	/**
	 * Returns 1 record
	 * @param Id $id
	 * @return mixed
	 */
	public function findById(Id $id);

	/**
	 * Returns all found records
	 * @return Collection
	 */
	public function findAll();

	/**
	 * Persists Entity
	 * @param $entity
	 * @return mixed
	 */
	public function save($entity);

}
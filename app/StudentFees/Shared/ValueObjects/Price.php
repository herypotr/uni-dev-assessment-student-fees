<?php namespace StudentFees\Shared\ValueObjects;

use InvalidArgumentException;

class Price extends AbstractValueObject {

	/**
	 * @var int
	 */
	protected $value = 0;

	/**
	 * @param $value
	 * @throws \Exception
	 */
	public function __construct($value)
	{
		// check strings for decimals
		if (is_string($value))
		{
			$value = $this->processStringValue($value);
		}

		if (!is_int($value) && !is_float($value))
		{
			throw new InvalidArgumentException('Price requires to be string, integer or float.');
		}

		if ($value < 0)
		{
			throw new InvalidArgumentException('Price requires value greater than or equal 0.');
		}

		// turn into integer (coins)
		if (is_float($value))
		{
			$value = round($value * 100, 2);
		}

		$this->value = $value;
	}

	/**
	 * @param $value
	 * @return static
	 */
	public static function make($value)
	{
		return new static($value);
	}

	/**
	 * @return float
	 */
	public function asDecimal()
	{
	    return round($this->value / 100, 2);
	}

	/**
	 * @return int
	 */
	public function asInteger()
	{
	    return $this->value;
	}

	/**
	 * @param $value
	 * @return float|int
	 */
	private function processStringValue($value)
	{
		// check for decimals
		if (preg_match('/[\.]/', $value, $matches))
		{
			return (float)$value;
		}

		// check for rubbish
		if (!preg_match('/^[0-9]*$/', $value))
		{
			throw new InvalidArgumentException('Price can consist only of numbers and "." character');
		}

		// expecting that string represents pounds
		return (int)$value * 100;
	}
}
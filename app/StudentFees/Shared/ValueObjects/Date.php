<?php namespace StudentFees\Shared\ValueObjects;


use InvalidArgumentException;
use Carbon\Carbon;

class Date extends AbstractValueObject {

	/**
	 * @var Carbon
	 */
	protected $value;

	/**
	 * @param string $date format YYYY-MM-DD
	 * @throws \Exception
	 */
	public function __construct($date)
	{
		if (!preg_match('/[0-9]{4}\-[0-9]{2}\-[0-9]{2}/', $date))
		{
			throw new InvalidArgumentException('Date requires format YYYY-MM-DD');
		}

		list ($year, $month, $day) = explode('-', $date);
		if (!checkdate($month, $day, $year))
		{
			throw new InvalidArgumentException('Date requires actual valid date.');
		}

		$date = Carbon::createFromDate($year, $month, $day);

		$this->value = $date;
	}

	/**
	 * @param $value
	 * @return static
	 */
	public static function make($value)
	{
		return new static($value);
	}

	/**
	 * @return Carbon
	 */
	public function getRaw()
	{
		return $this->value;
	}

	/**
	 * @param string $format
	 * @return string
	 */
	public function format($format)
	{
	    return $this->value->format($format);
	}

	/**
	 * @return string
	 */
	public function __toString()
	{
		return $this->value->format('Y-m-d');
	}

}
<?php namespace StudentFees\Shared\ValueObjects;

abstract class AbstractValueObject {

	/**
	 * @var
	 */
	protected $value;

	/**
	 * @return string
	 */
	public function __toString()
	{
		return (string)$this->value;
	}

}
<?php namespace StudentFees\Shared\ValueObjects;


use InvalidArgumentException;

class Id extends AbstractValueObject {

	/**
	 * @var
	 */
	protected $value;

	/**
	 * @param $value
	 * @throws \Exception
	 */
	public function __construct($value)
	{
		if (!is_int($value) || $value <= 0)
		{
			throw new InvalidArgumentException('Id requires integer greater than 0.');
		}

		$this->value = $value;
	}

	/**
	 * @param $value
	 * @return static
	 */
	public static function make($value)
	{
	    return new static($value);
	}

	/**
	 * @return int
	 */
	public function asInteger()
	{
	    return (int)$this->value;
	}

}
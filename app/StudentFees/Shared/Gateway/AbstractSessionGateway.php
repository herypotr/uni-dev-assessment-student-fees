<?php namespace StudentFees\Shared\Gateway;

use Illuminate\Support\Facades\Session;

abstract class AbstractSessionGateway {

	/**
	 * @var string
	 */
	protected $sessionKey;

	/**
	 * Inserts new record, generates new id
	 * @param array $data
	 * @return array
	 */
	public function insert($data)
	{
		$collection = Session::get($this->sessionKey, []);

		// get last id and incrmenet
		$collectionIds = array_keys($collection);
		$data['id'] = (int)array_pop($collectionIds) + 1;
		$collection[$data['id']] = $data;

		Session::put($this->sessionKey, $collection);
		return $data;
	}

	/**
	 * Updates record by given id
	 * @param array$data
	 * @param int $id
	 * @return array mixed
	 */
	public function update($data, $id)
	{
		$collection = Session::get($this->sessionKey, []);

		$collection[$id] = $data;

		Session::put($this->sessionKey, $collection);
		return $data;
	}

	/**
	 * Returns 1 record
	 * @param int $id
	 * @return array
	 * @throws \Exception
	 */
	public function find($id)
	{
		$collection = Session::get($this->sessionKey, []);

		if (!isset($collection[$id])) throw new \Exception('Record #' . $id . ' not found.');

		return $collection[$id];
	}

	/**
	 * Returns all records
	 * @return array
	 */
	public function findAll()
	{
		$collection = Session::get($this->sessionKey, []);

		return $collection;
	}

	/**
	 * Removes record from collection and returns it
	 * @param int $id
	 * @return array
	 * @throws \Exception
	 */
	public function destroy($id)
	{
		$collection = Session::get($this->sessionKey, []);

		if (!isset($collection[$id])) throw new \Exception('Record #' . $id . ' not found.');

		$record = $collection[$id];
		unset($collection[$id]);

		Session::put($this->sessionKey, $collection);
		return $record;
	}

}
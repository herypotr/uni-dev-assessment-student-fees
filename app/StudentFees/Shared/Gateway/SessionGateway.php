<?php  namespace StudentFees\Shared\Gateway;

interface SessionGateway {

	/**
	 * Inserts new record, generates new id
	 * @param array $data
	 * @return array
	 */
	public function insert($data);

	/**
	 * Updates record by given id
	 * @param array$data
	 * @param int $id
	 * @return array mixed
	 */
	public function update($data, $id);

	/**
	 * Returns 1 record
	 * @param int $id
	 * @return array
	 * @throws \Exception
	 */
	public function find($id);

	/**
	 * Returns all records
	 * @return array
	 */
	public function findAll();

	/**
	 * Removes record from collection and returns it
	 * @param int $id
	 * @return array
	 * @throws \Exception
	 */
	public function destroy($id);

}
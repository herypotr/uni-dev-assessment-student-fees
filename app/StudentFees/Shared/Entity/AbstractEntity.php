<?php namespace StudentFees\Shared\Entity;


use ReflectionClass;
use ReflectionProperty;
use StudentFees\Shared\ValueObjects\Id;

abstract class AbstractEntity {

	/**
	 * @var array
	 */
	private $valueObjectsToDataTypes = [
		'StudentFees\Shared\ValueObjects\Id' => 'int',
	];

	/**
	 * @var Id
	 */
	protected $id;

	/**
	 * @return Id
	 */
	public function getId()
	{
		return $this->id;
	}

	/**
	 * @param Id $id
	 */
	public function setId(Id $id)
	{
		$this->id = $id;
	}

	/**
	 * Returns array of all public and protected values from an entity. Will not return private values
	 * @return array
	 */
	public function toArray()
	{
		$entityArray = array();
		$reflect     = new ReflectionClass($this);
		$props       = $reflect->getProperties(ReflectionProperty::IS_PUBLIC | ReflectionProperty::IS_PROTECTED);
		foreach ($props as $prop)
		{
			$propName               = $prop->getName();
			$entityArray[snake_case($propName)] = $this->{$propName};

			// special care of objects
			if (is_object($this->{$propName}))
			{
				$entityArray[snake_case($propName)] = $this->processEntityProperty($this->{$propName});
			}
		}

		return $entityArray;
	}

	/**
	 * Checks, whether property is in the list of various Value Object instances
	 * for substitution of data types
	 * @param $property
	 * @return int
	 */
	private function processEntityProperty($property)
	{
		$value = $property->__toString();

		foreach ($this->valueObjectsToDataTypes as $instanceOf => $dataType)
		{
			if (!($property instanceof $instanceOf)) continue;

			switch ($dataType)
			{
				case 'int':
					return (int) $value;
			}
		}

		return $value;
	}

}
<?php namespace StudentFees\Student;


use StudentFees\Shared\Entity\AbstractEntity;

class Student extends AbstractEntity {

	/**
	 * @var string
	 */
	protected $forename;

	/**
	 * @var string
	 */
	protected $surname;

	/**
	 * @return string
	 */
	public function getForename()
	{
		return $this->forename;
	}

	/**
	 * @param string $forename
	 */
	public function setForename($forename)
	{
		$this->forename = $forename;
	}

	/**
	 * @return string
	 */
	public function getSurname()
	{
		return $this->surname;
	}

	/**
	 * @param string $surname
	 */
	public function setSurname($surname)
	{
		$this->surname = $surname;
	}

}
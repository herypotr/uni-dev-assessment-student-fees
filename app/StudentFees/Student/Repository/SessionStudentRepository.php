<?php namespace StudentFees\Student\Repository;

use StudentFees\Shared\Repository\AbstractSessionRepository;
use StudentFees\Shared\ValueObjects\Id;
use StudentFees\Student\Gateway\SessionStudentGateway;
use StudentFees\Student\Student;
use StudentFees\Student\StudentRepository;

class SessionStudentRepository extends AbstractSessionRepository implements StudentRepository {

	/**
	 * @param SessionStudentGateway $gateway
	 */
	public function __construct(SessionStudentGateway $gateway)
	{
		$this->gateway = $gateway;
	}

	/**
	 * @param $record
	 * @return mixed
	 */
	protected function createEntityFromRecord($record)
	{
		$entity = new Student();

		$entity->setId(Id::make($record['id']));
		$entity->setForename($record['forename']);
		$entity->setSurname($record['surname']);

		return $entity;
	}

}
<?php namespace StudentFees\Student\Gateway;

use StudentFees\Shared\Gateway\AbstractSessionGateway;
use StudentFees\Shared\Gateway\SessionGateway;

class SessionStudentGateway extends AbstractSessionGateway implements SessionGateway {

	/**
	 * @var string
	 */
	protected $sessionKey = 'persistence.students';

}
<?php namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider {

	/**
	 * Bootstrap the application services.
	 *
	 * @return void
	 */
	public function boot()
	{
		//
	}

	/**
	 * Register the application services.
	 *
	 * @return void
	 */
	public function register()
	{
		$this->app->bind('StudentFees\Student\StudentRepository',
			'StudentFees\Student\Repository\SessionStudentRepository');

		$this->app->bind('StudentFees\Payment\PaymentRepository',
			'StudentFees\Payment\Repository\SessionPaymentRepository');
	}

}

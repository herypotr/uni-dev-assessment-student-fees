<?php namespace Tests\Unit\StudentFees\Shared\Gateway;

use StudentFees\Shared\Gateway\SessionGateway;
use TestCase;

class SessionGatewayTest extends TestCase {

	/**
	 * @var SessionGateway
	 */
	protected $gateway;

	public function testInsert()
	{
		$newRecord = $this->insertRecord();

		$this->assertEquals(1, $newRecord['id']);
	}

	public function testFind()
	{
		$newRecord = $this->insertRecord();

		$existingRecord = $this->gateway->find($newRecord['id']);
		$this->assertEquals($existingRecord['test'], $newRecord['test']);
	}

	public function testDestroy()
	{
		$newRecord = $this->insertRecord();
		$this->gateway->destroy($newRecord['id']);

		$this->setExpectedException('Exception');
		$this->gateway->find($newRecord['id']);
	}

	/**
	 * @return array
	 */
	public function insertRecord()
	{
		$newRecord = $this->gateway->insert([
			'test' => 'value'
		]);

		return $newRecord;
	}

}
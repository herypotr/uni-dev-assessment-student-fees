<?php namespace Tests\Unit\StudentFees\Shared\ValueObjects;

use StudentFees\Shared\ValueObjects\Id;
use TestCase;

class IdTest extends TestCase {

	public function testCreateInstance()
	{
		$this->assertInstanceOf('StudentFees\Shared\ValueObjects\Id', Id::make(1));
	}

	public function testFailWithZeroValue()
	{
		$this->setExpectedException('InvalidArgumentException');
		Id::make(0);
	}

	public function testFailWithNonIntegerValue()
	{
		$this->setExpectedException('InvalidArgumentException');
		Id::make('123');
	}

}

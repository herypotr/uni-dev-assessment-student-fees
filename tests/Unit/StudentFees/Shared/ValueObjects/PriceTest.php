<?php namespace Tests\Unit\StudentFees\Shared\ValueObjects;

use StudentFees\Shared\ValueObjects\Price;
use TestCase;

class PriceTest extends TestCase {

	public function testCreateFromInteger()
	{
		$price = Price::make(10);
		$this->assertEquals(10, $price->asInteger());
	}

	public function testCreateFromFloat()
	{
		$price = Price::make(10.05);
		$this->assertEquals(10.05, $price->asDecimal());
	}

	public function testCreateFromStringWithoutDecimals()
	{
		$price = Price::make('123');
		$this->assertEquals(123.00, $price->asDecimal());
	}

	public function testCreateFromStringWithDeciamls()
	{
		$price = Price::make('80.10');
		$this->assertEquals(8010, $price->asInteger());
	}

	public function testFailWithMinusZeroValue()
	{
		$this->setExpectedException('InvalidArgumentException');
		Price::make(-25);
	}

	public function testFailWithNoDigitStringValue()
	{
		$this->setExpectedException('InvalidArgumentException');
		Price::make('$123');
	}

	public function testFailWitDodgyDecimalStringValue()
	{
		$this->setExpectedException('InvalidArgumentException');
		Price::make('123,50');
	}

}

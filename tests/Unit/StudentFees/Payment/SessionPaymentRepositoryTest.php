<?php namespace Tests\Unit\StudentFees\Payment;


use Mockery;
use StudentFees\Shared\ValueObjects\Id;
use StudentFees\Shared\ValueObjects\Price;
use StudentFees\Shared\ValueObjects\Date;
use StudentFees\Payment\Repository\SessionPaymentRepository;
use StudentFees\Payment\Payment;
use TestCase;

class SessionPaymentRepositoryTest extends TestCase {

	protected $gateway;

	public function setUp()
	{
		$this->gateway = Mockery::mock('StudentFees\Payment\Gateway\SessionPaymentGateway');
	}

	public function tearDown()
	{
		parent::tearDown();
		Mockery::close();
	}

	public function testCreateInstance()
	{
		$this->assertInstanceOf('StudentFees\Payment\Repository\SessionPaymentRepository', new SessionPaymentRepository($this->gateway));
	}

	public function testPersistEntityWithId()
	{
		$existingPayment = $this->createPayment();

		$this->gateway->shouldReceive('update')->once()->andReturn([
			'id'         => 1,
			'student_id' => 1,
			'amount'     => 1000,
			'date'       => '2015-02-17',
		]);

		$repo            = new SessionPaymentRepository($this->gateway);
		$existingPayment = $repo->save($existingPayment);

		$this->assertEquals(1, $existingPayment->getId()->asInteger());
		$this->assertEquals(10.00, $existingPayment->getAmount()->asDecimal());
	}

	public function testPersistEntityWithoutId()
	{
		$newPayment = $this->createPayment(null);

		$this->gateway->shouldReceive('insert')->once()->andReturn([
			'id'         => 3,
			'student_id' => 1,
			'amount'     => 2800,
			'date'       => '2014-12-01',
		]);

		$repo       = new SessionPaymentRepository($this->gateway);
		$newPayment = $repo->save($newPayment);

		$this->assertEquals(3, $newPayment->getId()->asInteger());
		$this->assertEquals(1, $newPayment->getStudentId()->asInteger());
		$this->assertEquals(2800, $newPayment->getAmount()->asInteger());
		$this->assertEquals('2014-12-01', (string) $newPayment->getDate());
	}

	public function testRetrieveEntity()
	{
		$this->gateway->shouldReceive('find')->with(2)->andReturn([
			'id'         => 2,
			'student_id' => 1,
			'amount'     => 1500,
			'date'       => '2014-06-03',
		]);
		$repo = new SessionPaymentRepository($this->gateway);

		$payment = $repo->findById(Id::make(2));

		$this->assertEquals(2, $payment->getId()->asInteger());
		$this->assertEquals(15.00, $payment->getAmount()->asDecimal());
	}

	/**
	 * @param int $id
	 * @param int $amount
	 * @param string $date
	 * @param null $studentId
	 * @return Payment
	 * @internal param string $forename
	 * @internal param string $surname
	 */
	private function createPayment($id = 1, $amount = 1000, $date = '2015-02-17', $studentId = 1)
	{
		$payment = new Payment();

		if ($id) $payment->setId(Id::make($id));
		if ($studentId) $payment->setStudentId(Id::make($studentId));
		if ($amount) $payment->setAmount(Price::make($amount));
		if ($date) $payment->setDate(Date::make($date));

		return $payment;
	}

}
<?php namespace Tests\Unit\StudentFees\Payment;

use StudentFees\Shared\ValueObjects\Date;
use StudentFees\Shared\ValueObjects\Id;
use StudentFees\Payment\Payment;
use StudentFees\Shared\ValueObjects\Price;
use TestCase;

class PaymentTest extends TestCase {

	public function testCreateEmptyEntity()
	{
		$payment = new Payment();
		$this->assertInstanceOf('StudentFees\Payment\Payment', $payment);
		$this->assertEquals(null, $payment->getId());
	}

	public function testCreateEntity()
	{
		$payment = new Payment();
		$payment->setId(Id::make(1));
		$payment->setAmount(Price::make(9000));
		$payment->setDate(Date::make('2014-06-03'));

		$this->assertEquals(1, $payment->getId()->asInteger());
		$this->assertEquals(9000, $payment->getAmount()->asInteger());
		$this->assertEquals('2014-06-03', (string)$payment->getDate());
	}

}

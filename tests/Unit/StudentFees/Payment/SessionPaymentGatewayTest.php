<?php namespace Tests\Unit\StuentFees\Student;

use StudentFees\Payment\Gateway\SessionPaymentGateway;
use StudentFees\Shared\ValueObjects\Id;
use Tests\Unit\StudentFees\Shared\Gateway\SessionGatewayTest;

class SessionPaymentGatewayTest extends SessionGatewayTest {

	/**
	 * @var SessionPaymentGateway
	 */
	protected $gateway;

	public function __construct()
	{
		$this->gateway = new SessionPaymentGateway();
	}

	public function testFindByStudentId()
	{
		// prepare data
		$joe = $this->gateway->insert([
			'student_id' => 1,
			'name'       => 'Joe',
		]);

		$jane = $this->gateway->insert([
			'student_id' => 2,
			'name'       => 'Jane',
		]);

		$testStudents = $this->gateway->findByStudentId(Id::make(1));
		$student      = array_pop($testStudents);

		$this->assertEquals($joe['name'], $student['name']);

		$testStudents = $this->gateway->findByStudentId(Id::make(2));
		$student      = array_pop($testStudents);

		$this->assertEquals($jane['name'], $student['name']);
	}

}
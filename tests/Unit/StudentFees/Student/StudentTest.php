<?php namespace Tests\Unit\StudentFees\Student;

use StudentFees\Shared\ValueObjects\Id;
use StudentFees\Student\Student;
use TestCase;

class StudentTest extends TestCase {

	public function testCreateEmptyEntity()
	{
		$student = new Student();
		$this->assertInstanceOf('StudentFees\Student\Student', $student);
		$this->assertEquals(null, $student->getId());
	}

	public function testCreateEntity()
	{
	    $student = new Student();
		$student->setId(Id::make(1));
		$student->setForename('Flint');
		$student->setSurname('Lockwood');

		$this->assertEquals(1, $student->getId()->asInteger());
		$this->assertEquals('Flint', $student->getForename());
		$this->assertEquals('Lockwood', $student->getSurname());
	}

}

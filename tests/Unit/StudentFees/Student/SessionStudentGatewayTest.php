<?php namespace Tests\Unit\StudentFees\Student;

use StudentFees\Student\Gateway\SessionStudentGateway;
use Tests\Unit\StudentFees\Shared\Gateway\SessionGatewayTest;

class SessionStudentGatewayTest extends SessionGatewayTest {

	/**
	 * @var SessionStudentGateway
	 */
	protected $gateway;

	public function __construct()
	{
	    $this->gateway = new SessionStudentGateway();
	}

}
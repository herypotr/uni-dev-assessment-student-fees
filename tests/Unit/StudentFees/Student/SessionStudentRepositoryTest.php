<?php namespace Tests\Unit\StudentFees\Student;


use Mockery;
use StudentFees\Shared\ValueObjects\Id;
use StudentFees\Student\Repository\SessionStudentRepository;
use StudentFees\Student\Student;
use TestCase;

class SessionStudentRepositoryTest extends TestCase {

	protected $gateway;

	public function setUp()
	{
		$this->gateway = Mockery::mock('StudentFees\Student\Gateway\SessionStudentGateway');
	}

	public function tearDown()
	{
		parent::tearDown();
		Mockery::close();
	}

	public function testCreateInstance()
	{
		$this->assertInstanceOf('StudentFees\Student\Repository\SessionStudentRepository', new SessionStudentRepository($this->gateway));
	}

	public function testPersistEntityWithId()
	{
		$existingStudent = $this->createStudent();

		$this->gateway->shouldReceive('update')->once()->andReturn([
			'id'       => 1,
			'forename' => 'Flint',
			'surname'  => 'Lockwood',
		]);

		$repo            = new SessionStudentRepository($this->gateway);
		$existingStudent = $repo->save($existingStudent);

		$this->assertEquals(1, $existingStudent->getId()->asInteger());
		$this->assertEquals('Flint', $existingStudent->getForename());
	}

	public function testPersistEntityWithoutId()
	{
		$newStudent = $this->createStudent(null);

		$this->gateway->shouldReceive('insert')->once()->andReturn([
			'id'       => 3,
			'forename' => 'Flint',
			'surname'  => 'Lockwood',
		]);

		$repo       = new SessionStudentRepository($this->gateway);
		$newStudent = $repo->save($newStudent);

		$this->assertEquals(3, $newStudent->getId()->asInteger());
		$this->assertEquals('Flint', $newStudent->getForename());
	}

	public function testRetrieveEntity()
	{
		$this->gateway->shouldReceive('find')->with(2)->andReturn([
			'id'       => 2,
			'forename' => 'John',
			'surname'  => 'Smith',
		]);
		$repo = new SessionStudentRepository($this->gateway);

		$student = $repo->findById(Id::make(2));

		$this->assertEquals(2, $student->getId()->asInteger());
		$this->assertEquals('John', $student->getForename());
	}

	/**
	 * @param int $id
	 * @param string $forename
	 * @param string $surname
	 * @return Student
	 */
	private function createStudent($id = 1, $forename = 'Flint', $surname = 'Lockwood')
	{
		$student = new Student();

		if ($id) $student->setId(Id::make($id));
		if ($forename) $student->setForename($forename);
		if ($surname) $student->setSurname($surname);

		return $student;
	}

}